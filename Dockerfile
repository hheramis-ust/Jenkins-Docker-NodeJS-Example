FROM node:latest

ENV PROJECTDIR /nodeApp

WORKDIR $PROJECTDIR

COPY package*.json ./

RUN npm install

RUN echo "Copy files from local to container"

COPY . .

EXPOSE 3000

CMD ["npm", "start"]
